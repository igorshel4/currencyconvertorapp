//
//  ViewModal.swift
//  CurrencyConvertorApp
//
//  Created by Igor Shelepov on 16.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import Foundation

class ViewModel {
    var rate: Double?
    var yestedayRate: Double?
    var rateStr: String?
    var sourseCurrency = Currency.USD
    var targetCurrency = Currency.RUB
    var rateDif: Double?
    var rateDifStr: String?
    var updatedTimeStr: String?
    var errorStr: String?
    var convertor: Convertor!
    
    init() {
       convertor  = Convertor.shared
    }
    
    var currencyStr: String {return "\(sourseCurrency.rawValue) → \(targetCurrency.rawValue)"}
    
    func refreshData(_ observer: @escaping () -> ()) {
        errorStr = nil
        let netHelper: NetworkHelperProtocol = NetworkHelper.shared
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        DispatchQueue.global().async(group: dispatchGroup) {
        netHelper.getRates(date: nil) { [weak self] rateResult in
                switch (rateResult) {
                   case .success(let rates):
                       self?.convertor.carrencyRates = rates
                       self?.prepareRateStr()
                       self?.updatedTimeStr = Messages.shared.updatedTimeStr
                   case .failure(let error):
                       self?.errorStr = error.localizedDescription
                       self?.prepareRateStr()
                       self?.prepareRateDifString()
                }
                dispatchGroup.leave()
            }
        }
        dispatchGroup.enter()
         DispatchQueue.global().async(group: dispatchGroup) {
         netHelper.getRates(date: Date.getYesterday()) { [weak self] (yesterdayRateResult) in
             switch (yesterdayRateResult) {
                 case .success(let yesterdayRates):
                     self?.convertor.yesterdayCarrencyRates = yesterdayRates
                 case .failure(let error):
                     self?.errorStr = error.localizedDescription
                     self?.prepareRateDifString()
                }
                dispatchGroup.leave()
             }
         }
        dispatchGroup.notify(queue: DispatchQueue.main) {
            [weak self] in
            self?.prepareRateDifString ()
            observer()
        }

        

    }
    
    private func prepareRateStr() {
        guard !convertor.carrencyRates.isEmpty else { return }
        rate = convertor.convert(source: sourseCurrency, target: targetCurrency)
        guard   let rate = self.rate else { return }
        rateStr = rate.rateString
    }
    
    private func prepareRateDifString() {
        guard !convertor.yesterdayCarrencyRates.isEmpty else { return }
        yestedayRate =  convertor.convert(source: sourseCurrency, target: targetCurrency, yesterday: true)
        guard let yestedayRate = yestedayRate else { return }
        guard let rate = self.rate else { return }
        rateDif = (rate - yestedayRate) / yestedayRate * 100.0
        guard let rateDif = self.rateDif else { return }
        rateDifStr = Messages.shared.rateChangeInfo(rateDif: rateDif, currency: sourseCurrency)
    }
}
