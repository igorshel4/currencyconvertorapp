//
//  ViewController.swift
//  CurrencyConvertorApp
//
//  Created by Igor Shelepov on 15.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblRateInfo: UILabel!
    @IBOutlet var btnMenu: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var lblUpdateTime: UILabel!
    
    private enum SettingKeys: String {
        case SOURSE
        case TARGET
    }
        
    var menuViewController: MenuViewController?
    var viewModel = ViewModel()
    let greenColor = UIColor(red: 126 / 255, green: 211 / 255, blue: 33 / 255, alpha: 1.0)
    
    //MARK: - LifeCtcle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
        menuViewController?.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        getSettings() 
        lblRate.text = ""
        lblRateInfo.text = ""
        lblUpdateTime.text = ""
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    //MARK: - Private methods
    
    //get sourse and tagers currency from UserDefaults
    private func getSettings() {
        let defaults = UserDefaults.standard
        guard let sourse = defaults.string(forKey: SettingKeys.SOURSE.rawValue),
            let target = defaults.string(forKey: SettingKeys.TARGET.rawValue),
            let sourseCurrency = Currency(rawValue: sourse),
            let targetCurrency = Currency(rawValue: target)
            else { return }
        viewModel.sourseCurrency = sourseCurrency
        viewModel.targetCurrency = targetCurrency

    }
    
    private func updateInfo() {
        spinner.startAnimating()
        lblCurrency.setSpasingText(viewModel.currencyStr, 1)
        viewModel.refreshData { [weak self] in
            guard let `self` = self else { return }
                self.spinner.stopAnimating()
                if let errorStr = self.viewModel.errorStr {
                    Info.shared.showErrorMessage(message: errorStr, vc: self)
                }
                guard let rateStr = self.viewModel.rateStr else { return }
                self.lblRate.setSpasingText(rateStr, -4)
                self.lblRateInfo.text = "\(self.viewModel.rateDifStr ?? "")"
                if let rateDif = self.viewModel.rateDif {
                    self.lblRateInfo.textColor = rateDif >= 0 ? self.greenColor : .red
                }
                self.lblUpdateTime.setSpasingText("\(self.viewModel.updatedTimeStr ?? "")", 2)
        }
    }

    @objc func applicationDidBecomeActive() {
        updateInfo()
    }
    
    //MARK: - Actions
    
    @IBAction func btnMenuAction(_ sender: Any) {
        guard let menuVC = self.menuViewController else { return }
        menuVC.sourseCurrency = viewModel.sourseCurrency
        menuVC.targetCurrency = viewModel.targetCurrency
        menuVC.callback = { [weak self] sourse, target in
            self?.viewModel.sourseCurrency = sourse
            self?.viewModel.targetCurrency = target
            
            //save sourse and tagers currency to UserDefaults
            let defaults = UserDefaults.standard
            defaults.set(sourse.rawValue, forKey: SettingKeys.SOURSE.rawValue)
            defaults.set(target.rawValue, forKey: SettingKeys.TARGET.rawValue)

            self?.updateInfo()
        }
        UIView.animate(withDuration: 0.3) {
            self.menuViewController?.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            let currentWindow: UIWindow? = UIApplication.shared.keyWindow
            currentWindow?.addSubview(menuVC.view)
        }
    }
}

