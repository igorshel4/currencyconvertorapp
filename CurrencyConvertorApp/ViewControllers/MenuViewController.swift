//
//  MenuViewController.swift
//  CurrencyConvertorApp
//
//  Created by Igor Shelepov on 16.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    
    @IBOutlet weak var btnUsdRub: UIButton!
    @IBOutlet weak var btnUsdEur: UIButton!
    @IBOutlet weak var btnEurRub: UIButton!
    @IBOutlet weak var btnEurUsd: UIButton!
    @IBOutlet weak var btnRubUsd: UIButton!
    @IBOutlet weak var btnRubEur: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    
    var tapGesture: UITapGestureRecognizer!
    
    var sourseCurrency = Currency.USD
    var targetCurrency = Currency.RUB
    
    let unselectColor = UIColor(displayP3Red: 255, green: 255, blue: 255, alpha: 0.7)
    let buttonUnselectedFont = UIFont(name: "Lato-Regular", size: 28)
    let buttonSelectedFont = UIFont(name: "Lato-Black", size: 28)
    
    var callback: ((_ sourse: Currency, _ target: Currency) -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - LifeCtcle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(MenuViewController.viewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
        setButtonTextColor()
    }
    
    deinit {
        print("Menu controller free")
    }
    
    //MARK: - Private methods
    
    @objc func viewTapped(_ sender: UITapGestureRecognizer) {
        closeMenu()
    }
    
    private func setButtonTextColor() {
        for subview in stackView.subviews {
            if let button = subview as? UIButton {
                button.titleLabel?.font = buttonUnselectedFont
                button.setTitleColor(unselectColor, for: .normal)
            }
        }
        switch (sourseCurrency, targetCurrency) {
            case (Currency.USD, Currency.RUB):
                btnUsdRub.setTitleColor(.white, for: .normal)
                btnUsdRub.titleLabel?.font = buttonSelectedFont
            case (Currency.USD, Currency.EUR):
                btnUsdEur.setTitleColor(.white, for: .normal)
                btnUsdEur.titleLabel?.font = buttonSelectedFont
            case (Currency.EUR, Currency.RUB):
                btnEurRub.setTitleColor(.white, for: .normal)
                btnEurRub.titleLabel?.font = buttonSelectedFont
            case (Currency.EUR, Currency.USD):
                btnEurUsd.setTitleColor(.white, for: .normal)
                btnEurUsd.titleLabel?.font = buttonSelectedFont
            case (Currency.RUB, Currency.USD):
                btnRubUsd.setTitleColor(.white, for: .normal)
                btnRubUsd.titleLabel?.font = buttonSelectedFont
            case (Currency.RUB, Currency.EUR):
                btnRubEur.setTitleColor(.white, for: .normal)
                btnRubEur.titleLabel?.font = buttonSelectedFont
            default: break
        }
    }
    
    
    private func closeMenu() {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        }) { (finished) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }
    
    //MARK: - Actions

    @IBAction func btnUsdRubAction(_ sender: Any) {
        if let callback = callback {
            callback(Currency.USD, Currency.RUB)
        }
        closeMenu()
    }
    

    @IBAction func btnUsdEurAction(_ sender: Any) {
        if let callback = callback {
            callback(Currency.USD, Currency.EUR)
        }
        closeMenu()
    }
    
    
    @IBAction func btnEurRubAction(_ sender: Any) {
        if let callback = callback {
            callback(Currency.EUR, Currency.RUB)
        }
        closeMenu()
    }
    
    
    @IBAction func btnEurUsdAction(_ sender: Any) {
        if let callback = callback {
            callback(Currency.EUR, Currency.USD)
        }
        closeMenu()
    }
    
    @IBAction func btnRubUsdAction(_ sender: Any) {
        if let callback = callback {
            callback(Currency.RUB, Currency.USD)
        }
        closeMenu()
    }
    
    @IBAction func btnRubEurAction(_ sender: Any) {
        if let callback = callback {
            callback(Currency.RUB, Currency.EUR)
        }
        closeMenu()
    }
}
