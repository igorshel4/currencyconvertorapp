//
//  Double+String.swift
//  CurrencyConvertorApp
//
//  Created by Igor Shelepov on 20.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import Foundation

extension Double {
    var rateString: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.locale = NSLocale(localeIdentifier: Bundle.main.preferredLocalizations.first == "ru" ? "ru_Ru" : "en_US") as Locale
        return formatter.string(from: NSNumber(value: self)) ?? ""
    }
}
