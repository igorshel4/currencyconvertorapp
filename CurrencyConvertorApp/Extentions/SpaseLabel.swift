//
//  SpaseLabel.swift
//  CurrencyConvertorApp
//
//  Created by Igor Shelepov on 18.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import UIKit

extension UILabel {
    
    func setSpasingText(_ text: String, _ spase: Int) {
        let style = [NSAttributedString.Key.kern: spase]
        let attributeString = NSMutableAttributedString(string: text, attributes: style)
        self.attributedText = attributeString
    }
}
