//
//  String+Date.swift
//  CurrencyConvertorApp
//
//  Created by Igor Shelepov on 16.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import Foundation

extension Date {
    
    func toDateString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let dateString = dateFormatter.string(from: self)
        return dateString
    }
    
    func toTimeStrig() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let dateString = dateFormatter.string(from: self)
        return dateString
    }
    
    static func getYesterday() -> Date {
        let calendar = Calendar.current
        let today = Date()
        let midnight = calendar.startOfDay(for: today)
        let yesterday = calendar.date(byAdding: .day, value: -1, to: midnight)!
        return yesterday
    }
    
    
}
