//
//  Convertor.swift
//  CyrrencyConvertor
//
//  Created by Igor Shelepov on 15.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import UIKit

struct Convertor {
    private  init(){}
    static var shared = Convertor()
    var carrencyRates: [CurrencyRate] = [CurrencyRate]()
    var yesterdayCarrencyRates: [CurrencyRate] = [CurrencyRate]()
    
    func convert(source: Currency, target: Currency, yesterday: Bool = false) -> Double? {
        guard let sourseRate = getRate(currency: source, yesterday: yesterday), let targetRate = getRate(currency: target, yesterday: yesterday) else { return nil }
        return targetRate / sourseRate
    }
    
    func getRate(currency: Currency, yesterday: Bool = false) -> Double? {
        let rates = yesterday ? yesterdayCarrencyRates : carrencyRates
        return rates.filter{$0.currency == currency}.first?.rate
    }
}
