//
//  Currency.swift
//  CyrrencyConvertor
//
//  Created by Igor Shelepov on 15.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import Foundation

enum Currency: String, CaseIterable {
    case EUR, USD, RUB
}
