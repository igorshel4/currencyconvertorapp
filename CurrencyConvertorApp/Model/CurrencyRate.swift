//
//  CurrencyRate.swift
//  CyrrencyConvertor
//
//  Created by Igor Shelepov on 15.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import UIKit

struct CurrencyRate {
    let currency: Currency
    let rate: Double
}
