//
//  RateError.swift
//  CurrencyConvertorApp
//
//  Created by Igor Shelepov on 18.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import Foundation

enum RateError {
    case noInternetConnection
    case serverError
}

extension RateError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .noInternetConnection:
            return NSLocalizedString(Messages.shared.noInternetConnection, comment: "")
        case .serverError:
            return NSLocalizedString(Messages.shared.serverError, comment: "")
        }

    }
}
