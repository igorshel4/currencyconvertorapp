//
//  Info.swift
//  CurrencyConvertorApp
//
//  Created by Igor Shelepov on 18.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import UIKit

struct Info {
    
    static var shared = Info()
    private init() {}
    
    func showErrorMessage (message: String, vc: UIViewController) {
        let alert = UIAlertController(title: Messages.shared.error, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
}
