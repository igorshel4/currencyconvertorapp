//
//  NetworkHelper.swift
//  CurrencyConvertorApp
//
//  Created by Igor Shelepov on 15.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import UIKit

enum RateResult {
    case success([CurrencyRate])
    case failure(Error)
}

protocol NetworkHelperProtocol {
//    func getRates(date: Date?, callback: @escaping (_ result: [CurrencyRate]?, _ error: Error?) -> ())
    func getRates(date: Date?, callback: @escaping (RateResult) -> ())
}

struct NetworkHelper:  NetworkHelperProtocol {
    
    private init() {}
    static var shared  = NetworkHelper()
    
    let baseUrl = "http://data.fixer.io"
    let apiPath = "/api/"
    let accessKey = "6ba54b9307e88aea9c5c14fa4345451e"
    
    func getRates(date: Date?, callback: @escaping (RateResult) -> ()) {
        guard CheckInternet.Connection() else {
            callback(.failure(RateError.noInternetConnection))
            return
        }
        let dateStr = date == nil ? "latest" : date!.toDateString()
        let urlString  = "\(baseUrl)\(apiPath)\(dateStr)?access_key=\(accessKey)&symbols=\(Currency.allCases.map{$0.rawValue}.joined(separator: ","))"
        guard let url = URL(string: urlString) else {
            callback(.failure(RateError.serverError))
            return
        }
        URLSession.shared.dataTask(with: url) { (data, responce, error) in
            guard let data = data, error == nil else {
                callback(.failure(RateError.serverError))
                return
            }
            let json = try! JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            guard let rates = json?["rates"] as? [String: Any] else {
                callback(.failure(RateError.serverError))
                return
            }
            var currencyRate = [CurrencyRate]()
            for (key, value) in rates {
                guard let currency = Currency(rawValue: key), let rate = value as? Double else { continue }
                currencyRate.append(CurrencyRate(currency: currency, rate: rate))
            }
            callback(.success(currencyRate))
        }.resume()
    }
}
