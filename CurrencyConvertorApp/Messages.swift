//
//  Messages.swift
//  CurrencyConvertorApp
//
//  Created by Igor Shelepov on 17.07.2020.
//  Copyright © 2020 Sharp Dev. All rights reserved.
//

import Foundation

struct Messages {
    private init() {}
    static var shared = Messages()
    let isRusLocale = Bundle.main.preferredLocalizations.first == "ru"
    
    var error: String {
        return Bundle.main.localizedString(forKey: "Error", value: "Error", table: nil )
    }
    
    var serverError: String {
        return Bundle.main.localizedString(forKey: "Server error", value: "Server error", table: nil )
    }
    
    var noInternetConnection: String {
        return Bundle.main.localizedString(forKey: "No internet connection", value: "No internet connection", table: nil )
    }
    
    
    func rateChangeInfo(rateDif: Double, currency: Currency) -> String {
        if rateDif == 0 {
            return Bundle.main.localizedString(forKey: "Rate not changed from yesterday", value: "Rate not changed from yesterday", table: nil )
        }
        let roundedRateDif = Int(round(rateDif))
        if abs(roundedRateDif) == 0 {
            return Bundle.main.localizedString(forKey: "Since yesterday", value: "Since yesterday", table: nil )
                + " "
                + currencyName(currency)
                + " "
                + upDownStr(rateDif)
                + " "
                + Bundle.main.localizedString(forKey: "less than 1 procent", value: "less than 1 procent", table: nil )
        }
        return Bundle.main.localizedString(forKey: "Since yesterday", value: "Since yesterday", table: nil )
            + " "
            + currencyName(currency)
            + " "
            + upDownStr(rateDif)
            + " "
            + Bundle.main.localizedString(forKey: "by", value: "by", table: nil )
            + " \(abs(roundedRateDif)) "
            + procentStr(roundedRateDif)

    }
    
    var updatedTimeStr: String {
        return String(format: NSLocalizedString("UPDATED AT time", comment: ""), (Date().toTimeStrig()))
    }
    
    private func currencyName(_ currency: Currency) -> String {
        switch (currency) {
            case .RUB: return Bundle.main.localizedString(forKey: "ruble", value: "ruble", table: nil )
            case .USD: return Bundle.main.localizedString(forKey: "dollar", value: "dollar", table: nil )
            case .EUR: return Bundle.main.localizedString(forKey: "euro", value: "euro", table: nil )
        }
    }
    
    private func procentStr(_ rateDif: Int) -> String {
        let absRate = abs(rateDif)
        if absRate == 1 {
            return isRusLocale ? "процент" : "precent"
        }
        if absRate % 10 == 1 && absRate % 100 != 11 {
            return isRusLocale ? "процент" : "precents"
        }
        if (absRate % 10 == 2 && absRate % 100 != 12)
            || (absRate % 10 == 3 && absRate % 100 != 13)
            || (absRate % 10 == 4 && absRate % 100 != 14) {
            return isRusLocale ? "процента" : "precents"
        }
        return isRusLocale ? "процентов" : "precents"
    }
    
    
    private func upDownStr(_ rateDif: Double) -> String {
        if rateDif > 0 {
            return Bundle.main.localizedString(forKey: "increased", value: "increased", table: nil )
        } else {
            return Bundle.main.localizedString(forKey: "fell", value: "fell", table: nil )
        }
    }
}
